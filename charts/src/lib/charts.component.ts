import { Component, Input, TemplateRef, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExtandDialogComponent } from '@his-directive/extand-dialog/dist/extand-dialog';
import { Coding } from '@his-base/datatypes';
import '@his-base/date-extension';
//primeng
import { ChartModule } from 'primeng/chart';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { TooltipModule } from 'primeng/tooltip';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
const primeGroup = [ChartModule, ButtonModule, TableModule, DropdownModule, TooltipModule, ScrollPanelModule]

@Component({
  selector: 'his-charts',
  standalone: true,
  imports: [FormsModule, CommonModule, ExtandDialogComponent, TranslateModule, primeGroup],
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartComponent {

  /** 折線圖資料 & 設定 */
  @Input() chartList: any;
  @Input() chartOptions: any;

  /** card標題 */
  @Input() chartTitle: string = '';

  /** 表格標題 & 資料 */
  @Input() chartTableList: Object[] = [];

  /** 日期格式 */
  @Input() dateFormatString: string = '';

  /** 下拉式選單資料 */
  @Input() options: Coding[] = [];

  isVisible: boolean = false;
  selectedTemplate!: TemplateRef<any>;
  selectedOption: Coding | undefined;

  #translate: TranslateService = inject(TranslateService);

  /**
   * 取出資料的title
   * @returns 表格標題
   */
  getTableHeaders(): any {
    if (this.chartTableList && this.chartTableList.length > 0) {
      let headerKeys = Object.keys(this.chartTableList[0]);
      headerKeys = headerKeys.map(x => x);
      return headerKeys;
    } else {
      return [];
    }
  }

  /**
   * 點選右上角按紐跳出視窗
   * @param selectedTemplate
   */
  onExtandDialog(selectedTemplate: TemplateRef<any>) {

    this.isVisible = true;
    this.selectedTemplate = selectedTemplate;
  }

  /**
   * 將日期轉成要呈現的資料型態
   * @param chartTableList
   * @returns 轉換過後的結果
   */
  formatValues(chartTableList:Object){

    return Object.values(chartTableList).map(x=>x instanceof Date ?(x as Date).formatString(this.dateFormatString) :x);
  }

  /**
   * 鼠標靠近表格呈現詳細內容
   * @param chartTableList
   * @returns 表格詳細內容
   */
  formatTooltip(rowItem: any): string {

    let tip: string = '';
    const headers = this.getTableHeaders(); // 获取表头
    if (headers && this.chartTableList) {
      headers.forEach((header: any, index: any) => {
        this.#translate.get(header).subscribe((translatedHeader: string) => {
          let value = Object.values(rowItem)[index];
          if (value instanceof Date) {
            value = (value as Date).formatString(this.dateFormatString);
          }
          tip += `${translatedHeader}: ${value}\n`;
        });
      });
    }
    return tip;
  }

}

