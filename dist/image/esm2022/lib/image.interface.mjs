/**
 ** 表格名稱：MedicalRecordImage (Medical Record Image)
 ** 表格說明：電子病歷圖檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/
export class DisplayImageContent {
    /** 建構式
      * @param that Patient
      */
    constructor(that) {
        /** 紀錄時間
          * @default new Date()
          */
        this.recordTime = new Date();
        /** 診別
          * @default ''
          */
        this.diagnosis = '';
        /** 體溫
          * @default ''
          */
        this.temperature = '';
        /** 呼吸
          * @default ''
          */
        this.breathe = '';
        /** 脈搏
          * @default ''
          */
        this.pulse = '';
        /** SBP
          * @default ''
          */
        this.SBP = '';
        /** DBP
          * @default ''
          */
        this.DBP = '';
        /** MAP
          * @default ''
          */
        this.MAP = '';
        /** AUG
          * @default ''
          */
        this.AUG = '';
        /** SPO2
          * @default ''
          */
        this.SPO2 = '';
        /** GCS
          * @default ''
          */
        this.GCS = '';
        Object.assign(this, structuredClone(that));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vaW1hZ2Uvc3JjL2xpYi9pbWFnZS5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7OztJQU1JO0FBRUgsTUFBTSxPQUFPLG1CQUFtQjtJQXlEL0I7O1FBRUk7SUFDSixZQUFZLElBQW1DO1FBMUQvQzs7WUFFSTtRQUNKLGVBQVUsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRTlCOztZQUVJO1FBQ0osY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUV2Qjs7WUFFSTtRQUNKLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBRXpCOztZQUVJO1FBQ0osWUFBTyxHQUFXLEVBQUUsQ0FBQztRQUVyQjs7WUFFSTtRQUNKLFVBQUssR0FBVyxFQUFFLENBQUM7UUFFbkI7O1lBRUk7UUFDSixRQUFHLEdBQVcsRUFBRSxDQUFDO1FBRWpCOztZQUVJO1FBQ0osUUFBRyxHQUFXLEVBQUUsQ0FBQztRQUVqQjs7WUFFSTtRQUNKLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFFakI7O1lBRUk7UUFDSixRQUFHLEdBQVcsRUFBRSxDQUFDO1FBRWpCOztZQUVJO1FBQ0osU0FBSSxHQUFXLEVBQUUsQ0FBQztRQUVsQjs7WUFFSTtRQUNKLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFPZixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqKiDooajmoLzlkI3nqLHvvJpNZWRpY2FsUmVjb3JkSW1hZ2UgKE1lZGljYWwgUmVjb3JkIEltYWdlKVxuICoqIOihqOagvOiqquaYju+8mumbu+WtkOeXheatt+WcluaqlFxuICoqIOe3qOioguS6uuWToe+8mueOi+mmqOaZqFxuICoqIOagoemWseS6uuWToe+8muWtq+WfueeEtlxuICoqIOioreioiOaXpeacn++8mlxuICoqL1xuXG4gZXhwb3J0IGNsYXNzIERpc3BsYXlJbWFnZUNvbnRlbnQge1xuXG4gIC8qKiDntIDpjITmmYLplpNcbiAgICAqIEBkZWZhdWx0IG5ldyBEYXRlKClcbiAgICAqL1xuICByZWNvcmRUaW1lOiBEYXRlID0gbmV3IERhdGUoKTtcblxuICAvKiog6Ki65YilXG4gICAgKiBAZGVmYXVsdCAnJ1xuICAgICovXG4gIGRpYWdub3Npczogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOmrlOa6q1xuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICB0ZW1wZXJhdHVyZTogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOWRvOWQuFxuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICBicmVhdGhlOiBzdHJpbmcgPSAnJztcblxuICAvKiog6ISI5pCPXG4gICAgKiBAZGVmYXVsdCAnJ1xuICAgICovXG4gIHB1bHNlOiBzdHJpbmcgPSAnJztcblxuICAvKiogU0JQXG4gICAgKiBAZGVmYXVsdCAnJ1xuICAgICovXG4gIFNCUDogc3RyaW5nID0gJyc7XG5cbiAgLyoqIERCUFxuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICBEQlA6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiBNQVBcbiAgICAqIEBkZWZhdWx0ICcnXG4gICAgKi9cbiAgTUFQOiBzdHJpbmcgPSAnJztcblxuICAvKiogQVVHXG4gICAgKiBAZGVmYXVsdCAnJ1xuICAgICovXG4gIEFVRzogc3RyaW5nID0gJyc7XG5cbiAgLyoqIFNQTzJcbiAgICAqIEBkZWZhdWx0ICcnXG4gICAgKi9cbiAgU1BPMjogc3RyaW5nID0gJyc7XG5cbiAgLyoqIEdDU1xuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICBHQ1M6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiDlu7rmp4vlvI9cbiAgICAqIEBwYXJhbSB0aGF0IFBhdGllbnRcbiAgICAqL1xuICBjb25zdHJ1Y3Rvcih0aGF0PzogUGFydGlhbDxEaXNwbGF5SW1hZ2VDb250ZW50Pikge1xuXG4gICAgT2JqZWN0LmFzc2lnbih0aGlzLCBzdHJ1Y3R1cmVkQ2xvbmUodGhhdCkpO1xuIH1cbn1cbiJdfQ==