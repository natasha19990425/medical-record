import * as i0 from '@angular/core';
import { Component, Input } from '@angular/core';
import * as i2 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i1 from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ExtandDialogComponent } from '@his-directive/extand-dialog/dist/extand-dialog';
import '@his-base/date-extension';
import * as i3 from 'primeng/chart';
import { ChartModule } from 'primeng/chart';
import * as i4 from 'primeng/button';
import { ButtonModule } from 'primeng/button';
import * as i6 from 'primeng/table';
import { TableModule } from 'primeng/table';
import * as i7 from 'primeng/dropdown';
import { DropdownModule } from 'primeng/dropdown';
import * as i8 from 'primeng/tooltip';
import { TooltipModule } from 'primeng/tooltip';
import * as i9 from 'primeng/scrollpanel';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import * as i5 from 'primeng/api';

const primeGroup = [ChartModule, ButtonModule, TableModule, DropdownModule, TooltipModule, ScrollPanelModule];
class ImageComponent {
    constructor() {
        /** card標題 */
        this.cardHeader = '';
        /** 表格標題 & 資料 */
        this.imageHeaders = [];
        this.imageList = [];
        /** 日期格式 */
        this.dateFormatString = '';
        /** 下拉式選單資料 */
        this.options = [];
        this.isVisible = false;
    }
    /**
     * 點選右上角按紐跳出視窗
     * @param selectedTemplate
     */
    onExtandDialog(selectedTemplate) {
        this.isVisible = true;
        this.selectedTemplate = selectedTemplate;
    }
    /**
     * 將日期轉成要呈現的資料型態
     * @param imageList
     * @returns 轉換過後的結果
     */
    keys(imageList) {
        return Object.values(imageList).map(x => x instanceof Date ? x.formatString(this.dateFormatString) : x);
    }
    /**
     * 鼠標靠近表格呈現詳細內容
     * @param imageList
     * @returns 表格詳細內容
     */
    tips(imageList) {
        let tip = '';
        if (this.imageHeaders && Array.isArray(this.imageHeaders)) {
            this.imageHeaders.forEach((header, index) => {
                const value = Object.values(imageList)[index];
                if (value !== undefined) {
                    if (value instanceof Date) {
                        const formattedDate = value.formatString(this.dateFormatString); // 例如，使用 toLocaleDateString()
                        tip += `${header}: ${formattedDate}\n`;
                    }
                    else {
                        tip += `${header}: ${value}\n`;
                    }
                }
            });
        }
        return tip;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.2.3", ngImport: i0, type: ImageComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "16.2.3", type: ImageComponent, isStandalone: true, selector: "his-image", inputs: { chartList: "chartList", chartOptions: "chartOptions", cardHeader: "cardHeader", imageHeaders: "imageHeaders", imageList: "imageList", dateFormatString: "dateFormatString", options: "options" }, ngImport: i0, template: "<div class=\"mycard card\">\n  <div class=\"text-title\">\n    <p-dropdown [options]=\"options\" [(ngModel)]=\"selectedOption\" [filter]=\"true\" filterBy=\"display\" [showClear]=\"true\" [placeholder]=\"cardHeader\">\n      <ng-template pTemplate=\"selectedItem\">\n        <div class=\"flex align-items-center gap-2\" *ngIf=\"selectedOption\">\n          <div>{{ selectedOption.display }}</div>\n        </div>\n      </ng-template>\n      <ng-template let-option pTemplate=\"item\">\n        <div class=\"flex align-items-center gap-2\">\n          <div>{{ option.display }}</div>\n        </div>\n      </ng-template>\n    </p-dropdown>\n    <p-button icon=\"pi pi-window-maximize\" styleClass=\"p-button-rounded p-button-text\" (click)=\"onExtandDialog(imageTemplate)\"></p-button>\n  </div>\n\n  <div class=\"flex-grow-1\">\n    <p-chart class=\"myChart\" [data]=\"chartList\" [options]=\"chartOptions \" height=\"25vh\" width=\"100%\"></p-chart>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"imageList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of imageHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n      </ng-template>\n    </p-table>\n  </p-scrollPanel>\n</div>\n\n<ng-template #imageTemplate>\n  <div class=\"flex-grow-1\">\n    <p-chart class=\"myChart\" [data]=\"chartList\" [options]=\"chartOptions \" height=\"25vh\" width=\"100%\"></p-chart>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"imageList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\" >\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of imageHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n      </ng-template>\n    </p-table>\n  </p-scrollPanel>\n</ng-template>\n<his-extand-dialog class=\"myDialog\"\n  [header]=\"cardHeader\"\n  [inputTemplate]=\"selectedTemplate\"\n  [(visible)]=\"isVisible\"\n  [width]=\"'40rem'\"\n  [height]=\"'30rem'\"\n></his-extand-dialog>\n", styles: [":host{height:100%;width:100%}.text-title{display:flex;justify-content:space-between}.mycard.card{padding:.5rem;height:100%;width:100%;overflow:auto}::ng-deep .p-datatable-table{margin-top:.5rem}::ng-deep .p-datatable-wrapper{overflow:unset!important;height:13vh;width:100%!important}\n"], dependencies: [{ kind: "ngmodule", type: FormsModule }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: ExtandDialogComponent, selector: "his-extand-dialog", inputs: ["inputTemplate", "header", "visible", "height", "width"], outputs: ["visibleChange"] }, { kind: "ngmodule", type: ChartModule }, { kind: "component", type: i3.UIChart, selector: "p-chart", inputs: ["type", "plugins", "width", "height", "responsive", "data", "options"], outputs: ["onDataSelect"] }, { kind: "ngmodule", type: ButtonModule }, { kind: "component", type: i4.Button, selector: "p-button", inputs: ["type", "iconPos", "icon", "badge", "label", "disabled", "loading", "loadingIcon", "style", "styleClass", "badgeClass", "ariaLabel"], outputs: ["onClick", "onFocus", "onBlur"] }, { kind: "directive", type: i5.PrimeTemplate, selector: "[pTemplate]", inputs: ["type", "pTemplate"] }, { kind: "ngmodule", type: TableModule }, { kind: "component", type: i6.Table, selector: "p-table", inputs: ["frozenColumns", "frozenValue", "style", "styleClass", "tableStyle", "tableStyleClass", "paginator", "pageLinks", "rowsPerPageOptions", "alwaysShowPaginator", "paginatorPosition", "paginatorStyleClass", "paginatorDropdownAppendTo", "paginatorDropdownScrollHeight", "currentPageReportTemplate", "showCurrentPageReport", "showJumpToPageDropdown", "showJumpToPageInput", "showFirstLastIcon", "showPageLinks", "defaultSortOrder", "sortMode", "resetPageOnSort", "selectionMode", "selectionPageOnly", "contextMenuSelection", "contextMenuSelectionMode", "dataKey", "metaKeySelection", "rowSelectable", "rowTrackBy", "lazy", "lazyLoadOnInit", "compareSelectionBy", "csvSeparator", "exportFilename", "filters", "globalFilterFields", "filterDelay", "filterLocale", "expandedRowKeys", "editingRowKeys", "rowExpandMode", "scrollable", "scrollDirection", "rowGroupMode", "scrollHeight", "virtualScroll", "virtualScrollItemSize", "virtualScrollOptions", "virtualScrollDelay", "frozenWidth", "responsive", "contextMenu", "resizableColumns", "columnResizeMode", "reorderableColumns", "loading", "loadingIcon", "showLoader", "rowHover", "customSort", "showInitialSortBadge", "autoLayout", "exportFunction", "exportHeader", "stateKey", "stateStorage", "editMode", "groupRowsBy", "groupRowsByOrder", "responsiveLayout", "breakpoint", "paginatorLocale", "value", "columns", "first", "rows", "totalRecords", "sortField", "sortOrder", "multiSortMeta", "selection", "selectAll", "virtualRowHeight"], outputs: ["contextMenuSelectionChange", "selectAllChange", "selectionChange", "onRowSelect", "onRowUnselect", "onPage", "onSort", "onFilter", "onLazyLoad", "onRowExpand", "onRowCollapse", "onContextMenuSelect", "onColResize", "onColReorder", "onRowReorder", "onEditInit", "onEditComplete", "onEditCancel", "onHeaderCheckboxToggle", "sortFunction", "firstChange", "rowsChange", "onStateSave", "onStateRestore"] }, { kind: "ngmodule", type: DropdownModule }, { kind: "component", type: i7.Dropdown, selector: "p-dropdown", inputs: ["scrollHeight", "filter", "name", "style", "panelStyle", "styleClass", "panelStyleClass", "readonly", "required", "editable", "appendTo", "tabindex", "placeholder", "filterPlaceholder", "filterLocale", "inputId", "selectId", "dataKey", "filterBy", "autofocus", "resetFilterOnHide", "dropdownIcon", "optionLabel", "optionValue", "optionDisabled", "optionGroupLabel", "optionGroupChildren", "autoDisplayFirst", "group", "showClear", "emptyFilterMessage", "emptyMessage", "lazy", "virtualScroll", "virtualScrollItemSize", "virtualScrollOptions", "overlayOptions", "ariaFilterLabel", "ariaLabel", "ariaLabelledBy", "filterMatchMode", "maxlength", "tooltip", "tooltipPosition", "tooltipPositionStyle", "tooltipStyleClass", "autofocusFilter", "overlayDirection", "disabled", "itemSize", "autoZIndex", "baseZIndex", "showTransitionOptions", "hideTransitionOptions", "filterValue", "options"], outputs: ["onChange", "onFilter", "onFocus", "onBlur", "onClick", "onShow", "onHide", "onClear", "onLazyLoad"] }, { kind: "ngmodule", type: TooltipModule }, { kind: "directive", type: i8.Tooltip, selector: "[pTooltip]", inputs: ["tooltipPosition", "tooltipEvent", "appendTo", "positionStyle", "tooltipStyleClass", "tooltipZIndex", "escape", "showDelay", "hideDelay", "life", "positionTop", "positionLeft", "autoHide", "fitContent", "hideOnEscape", "pTooltip", "tooltipDisabled", "tooltipOptions"] }, { kind: "ngmodule", type: ScrollPanelModule }, { kind: "component", type: i9.ScrollPanel, selector: "p-scrollPanel", inputs: ["style", "styleClass", "step"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.2.3", ngImport: i0, type: ImageComponent, decorators: [{
            type: Component,
            args: [{ selector: 'his-image', standalone: true, imports: [FormsModule, CommonModule, ExtandDialogComponent, primeGroup], template: "<div class=\"mycard card\">\n  <div class=\"text-title\">\n    <p-dropdown [options]=\"options\" [(ngModel)]=\"selectedOption\" [filter]=\"true\" filterBy=\"display\" [showClear]=\"true\" [placeholder]=\"cardHeader\">\n      <ng-template pTemplate=\"selectedItem\">\n        <div class=\"flex align-items-center gap-2\" *ngIf=\"selectedOption\">\n          <div>{{ selectedOption.display }}</div>\n        </div>\n      </ng-template>\n      <ng-template let-option pTemplate=\"item\">\n        <div class=\"flex align-items-center gap-2\">\n          <div>{{ option.display }}</div>\n        </div>\n      </ng-template>\n    </p-dropdown>\n    <p-button icon=\"pi pi-window-maximize\" styleClass=\"p-button-rounded p-button-text\" (click)=\"onExtandDialog(imageTemplate)\"></p-button>\n  </div>\n\n  <div class=\"flex-grow-1\">\n    <p-chart class=\"myChart\" [data]=\"chartList\" [options]=\"chartOptions \" height=\"25vh\" width=\"100%\"></p-chart>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"imageList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of imageHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n      </ng-template>\n    </p-table>\n  </p-scrollPanel>\n</div>\n\n<ng-template #imageTemplate>\n  <div class=\"flex-grow-1\">\n    <p-chart class=\"myChart\" [data]=\"chartList\" [options]=\"chartOptions \" height=\"25vh\" width=\"100%\"></p-chart>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"imageList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\" >\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of imageHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n      </ng-template>\n    </p-table>\n  </p-scrollPanel>\n</ng-template>\n<his-extand-dialog class=\"myDialog\"\n  [header]=\"cardHeader\"\n  [inputTemplate]=\"selectedTemplate\"\n  [(visible)]=\"isVisible\"\n  [width]=\"'40rem'\"\n  [height]=\"'30rem'\"\n></his-extand-dialog>\n", styles: [":host{height:100%;width:100%}.text-title{display:flex;justify-content:space-between}.mycard.card{padding:.5rem;height:100%;width:100%;overflow:auto}::ng-deep .p-datatable-table{margin-top:.5rem}::ng-deep .p-datatable-wrapper{overflow:unset!important;height:13vh;width:100%!important}\n"] }]
        }], propDecorators: { chartList: [{
                type: Input
            }], chartOptions: [{
                type: Input
            }], cardHeader: [{
                type: Input
            }], imageHeaders: [{
                type: Input
            }], imageList: [{
                type: Input
            }], dateFormatString: [{
                type: Input
            }], options: [{
                type: Input
            }] } });

/**
 ** 表格名稱：MedicalRecordImage (Medical Record Image)
 ** 表格說明：電子病歷圖檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/
class DisplayImageContent {
    /** 建構式
      * @param that Patient
      */
    constructor(that) {
        /** 紀錄時間
          * @default new Date()
          */
        this.recordTime = new Date();
        /** 診別
          * @default ''
          */
        this.diagnosis = '';
        /** 體溫
          * @default ''
          */
        this.temperature = '';
        /** 呼吸
          * @default ''
          */
        this.breathe = '';
        /** 脈搏
          * @default ''
          */
        this.pulse = '';
        /** SBP
          * @default ''
          */
        this.SBP = '';
        /** DBP
          * @default ''
          */
        this.DBP = '';
        /** MAP
          * @default ''
          */
        this.MAP = '';
        /** AUG
          * @default ''
          */
        this.AUG = '';
        /** SPO2
          * @default ''
          */
        this.SPO2 = '';
        /** GCS
          * @default ''
          */
        this.GCS = '';
        Object.assign(this, structuredClone(that));
    }
}

/*
 * Public API Surface of image
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DisplayImageContent, ImageComponent };
//# sourceMappingURL=his-view-image.mjs.map
