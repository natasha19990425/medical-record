import { TemplateRef } from '@angular/core';
import { Coding } from '@his-base/datatypes/dist/datatypes';
import '@his-base/date-extension';
import * as i0 from "@angular/core";
export declare class ImageComponent {
    /** 折線圖資料 & 設定 */
    chartList: any;
    chartOptions: any;
    /** card標題 */
    cardHeader: string;
    /** 表格標題 & 資料 */
    imageHeaders: string[] | undefined;
    imageList: Object[];
    /** 日期格式 */
    dateFormatString: string;
    /** 下拉式選單資料 */
    options: Coding[];
    isVisible: boolean;
    selectedTemplate: TemplateRef<any>;
    selectedOption: Coding | undefined;
    /**
     * 點選右上角按紐跳出視窗
     * @param selectedTemplate
     */
    onExtandDialog(selectedTemplate: TemplateRef<any>): void;
    /**
     * 將日期轉成要呈現的資料型態
     * @param imageList
     * @returns 轉換過後的結果
     */
    keys(imageList: Object): any[];
    /**
     * 鼠標靠近表格呈現詳細內容
     * @param imageList
     * @returns 表格詳細內容
     */
    tips(imageList: any): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<ImageComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ImageComponent, "his-image", never, { "chartList": { "alias": "chartList"; "required": false; }; "chartOptions": { "alias": "chartOptions"; "required": false; }; "cardHeader": { "alias": "cardHeader"; "required": false; }; "imageHeaders": { "alias": "imageHeaders"; "required": false; }; "imageList": { "alias": "imageList"; "required": false; }; "dateFormatString": { "alias": "dateFormatString"; "required": false; }; "options": { "alias": "options"; "required": false; }; }, {}, never, never, true, never>;
}
