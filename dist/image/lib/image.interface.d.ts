/**
 ** 表格名稱：MedicalRecordImage (Medical Record Image)
 ** 表格說明：電子病歷圖檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/
export declare class DisplayImageContent {
    /** 紀錄時間
      * @default new Date()
      */
    recordTime: Date;
    /** 診別
      * @default ''
      */
    diagnosis: string;
    /** 體溫
      * @default ''
      */
    temperature: string;
    /** 呼吸
      * @default ''
      */
    breathe: string;
    /** 脈搏
      * @default ''
      */
    pulse: string;
    /** SBP
      * @default ''
      */
    SBP: string;
    /** DBP
      * @default ''
      */
    DBP: string;
    /** MAP
      * @default ''
      */
    MAP: string;
    /** AUG
      * @default ''
      */
    AUG: string;
    /** SPO2
      * @default ''
      */
    SPO2: string;
    /** GCS
      * @default ''
      */
    GCS: string;
    /** 建構式
      * @param that Patient
      */
    constructor(that?: Partial<DisplayImageContent>);
}
