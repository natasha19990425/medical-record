/**
 ** 表格名稱：MedicalRecord (Medical Record)
 ** 表格說明：電子病歷檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/
export class SurgeryList {
    /** 建構式
      * @param that SurgeryList
      */
    constructor(that) {
        /** 手術日期
          * @default new Date()
          */
        this.surgeryDate = new Date();
        /** 手術方式
          * @default ''
          */
        this.surgeryWay = '';
        /** 麻醉方式
          * @default ''
          */
        this.anesthesiaWay = '';
        /** 科別
          * @default ''
          */
        this.department = '';
        /** 醫師
          * @default ''
          */
        this.doctor = '';
        Object.assign(this, structuredClone(that));
    }
}
export class InspectReport {
    /** 建構式
      * @param that InspectReport
      */
    constructor(that) {
        /** 狀態
          * @default ''
          */
        this.state = '';
        /** 報告名稱
          * @default ''
          */
        this.reportName = '';
        /** 診別
          * @default ''
          */
        this.diagnosis = '';
        /** 進入診間時間/到護理站時間/到護理站時間/開始日期
          * @default new Date()
          */
        this.plannedStartTime = new Date();
        Object.assign(this, structuredClone(that));
    }
}
export class MedicalDiagnosis {
    /** 建構式
      * @param that MedicalDiagnosis
      */
    constructor(that) {
        /** 診斷類別
           * @default ''
           */
        this.diagnosisSort = '';
        /** 診斷代碼
          * @default ''
          */
        this.diagnosticCode = '';
        /** 臨床診斷
          * @default ''
          */
        this.clinicalDiagnosis = '';
        Object.assign(this, structuredClone(that));
    }
}
export class CheckingReport {
    /** 建構式
      * @param that CheckingReport
      */
    constructor(that) {
        /** 狀態
          * @default ''
          */
        this.state = '';
        /** 報告名稱
          * @default ''
          */
        this.reportName = '';
        /** 申請項目
          * @default ''
          */
        this.applyProject = '';
        Object.assign(this, structuredClone(that));
    }
}
export class CurrentMedications {
    /** 建構式
      * @param that CurrentMedications
      */
    constructor(that) {
        /** 藥品學名
           * @default ''
           */
        this.drugName = '';
        /** 劑量單位
          * @default ''
          */
        this.dose = 0;
        /** 使用劑量
          * @default ''
          */
        this.dosage = '';
        /** 頻率
          * @default ''
          */
        this.frequency = '';
        Object.assign(this, structuredClone(that));
    }
}
export class NursingRecord {
    /** 建構式
      * @param that NursingRecord
      */
    constructor(that) {
        /** 紀錄時間
           * @default new Date()
           */
        this.recordTime = new Date();
        /** 焦點
          * @default ''
          */
        this.focus = '';
        Object.assign(this, structuredClone(that));
    }
}
export class ConsultationRecords {
    /** 建構式
      * @param that ConsultationRecords
      */
    constructor(that) {
        /** 狀態
           * @default ''
           */
        this.state = '';
        /** 紀錄時間
           * @default new Date()
           */
        this.recordTime = new Date();
        /** 科別
          * @default ''
          */
        this.department = '';
        Object.assign(this, structuredClone(that));
    }
}
export class ExamineCumulative {
    /** 建構式
      * @param that ExamineCumulative
      */
    constructor(that) {
        /** 報告日期
           * @default new Date()
           */
        this.reportDate = new Date();
        /** WBC
           * @default ''
           */
        this.WBC = '';
        /** Neotrophilic
          * @default ''
          */
        this.Neotrophilic = '';
        /** Lymphocytes
          * @default ''
          */
        this.Lymphocytes = '';
        /** Monocytes
          * @default ''
          */
        this.Monocytes = '';
        /** Basophils
          * @default ''
          */
        this.Basophils = '';
        /** NRBC
          * @default ''
          */
        this.NRBC = '';
        /** MPV
          * @default ''
          */
        this.MPV = '';
        Object.assign(this, structuredClone(that));
    }
}
export class MedicalRecords {
    /** 建構式
      * @param that MedicalRecords
      */
    constructor(that) {
        /** 就醫日期
           * @default new Date()
           */
        this.medicalDate = new Date();
        /** 科別
           * @default ''
           */
        this.department = '';
        /** 主治醫生
          * @default ''
          */
        this.doctor = '';
        Object.assign(this, structuredClone(that));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vdGFibGUvc3JjL2xpYi90YWJsZS5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7OztJQU1JO0FBR0osTUFBTSxPQUFPLFdBQVc7SUEyQnRCOztRQUVJO0lBQ0osWUFBWSxJQUEyQjtRQTVCdkM7O1lBRUk7UUFDSixnQkFBVyxHQUFTLElBQUksSUFBSSxFQUFFLENBQUM7UUFFL0I7O1lBRUk7UUFDSixlQUFVLEdBQVcsRUFBRSxDQUFDO1FBRXhCOztZQUVJO1FBQ0osa0JBQWEsR0FBVyxFQUFFLENBQUM7UUFFM0I7O1lBRUk7UUFDSixlQUFVLEdBQVcsRUFBRSxDQUFDO1FBRXhCOztZQUVJO1FBQ0osV0FBTSxHQUFXLEVBQUUsQ0FBQztRQU9sQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0Q7QUFFRCxNQUFNLE9BQU8sYUFBYTtJQXNCdkI7O1FBRUk7SUFDSixZQUFZLElBQTZCO1FBdkJ6Qzs7WUFFSTtRQUNKLFVBQUssR0FBVyxFQUFFLENBQUM7UUFFbkI7O1lBRUk7UUFDSixlQUFVLEdBQVcsRUFBRSxDQUFDO1FBRXhCOztZQUVJO1FBQ0osY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUV2Qjs7WUFFSTtRQUNKLHFCQUFnQixHQUFTLElBQUksSUFBSSxFQUFFLENBQUM7UUFPbEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNGO0FBRUQsTUFBTSxPQUFPLGdCQUFnQjtJQWlCM0I7O1FBRUk7SUFDSixZQUFZLElBQWdDO1FBbEI1Qzs7YUFFSztRQUNKLGtCQUFhLEdBQVcsRUFBRSxDQUFDO1FBRTNCOztZQUVJO1FBQ0osbUJBQWMsR0FBVyxFQUFFLENBQUM7UUFFNUI7O1lBRUk7UUFDSixzQkFBaUIsR0FBVyxFQUFFLENBQUM7UUFPOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNEO0FBRUQsTUFBTSxPQUFPLGNBQWM7SUFpQnpCOztRQUVJO0lBQ0osWUFBWSxJQUE4QjtRQWxCekM7O1lBRUk7UUFDSixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBRW5COztZQUVJO1FBQ0osZUFBVSxHQUFXLEVBQUUsQ0FBQztRQUV4Qjs7WUFFSTtRQUNKLGlCQUFZLEdBQVcsRUFBRSxDQUFDO1FBT3pCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Q0FDRDtBQUVELE1BQU0sT0FBTyxrQkFBa0I7SUFzQjlCOztRQUVJO0lBQ0osWUFBWSxJQUFrQztRQXZCN0M7O2FBRUs7UUFDSixhQUFRLEdBQVcsRUFBRSxDQUFDO1FBRXRCOztZQUVJO1FBQ0osU0FBSSxHQUFXLENBQUMsQ0FBQztRQUVqQjs7WUFFSTtRQUNKLFdBQU0sR0FBVyxFQUFFLENBQUM7UUFFcEI7O1lBRUk7UUFDSixjQUFTLEdBQVcsRUFBRSxDQUFDO1FBT3ZCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Q0FDQTtBQUVELE1BQU0sT0FBTyxhQUFhO0lBWXhCOztRQUVJO0lBQ0osWUFBWSxJQUE2QjtRQWJ6Qzs7YUFFSztRQUNKLGVBQVUsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRTlCOztZQUVJO1FBQ0osVUFBSyxHQUFXLEVBQUUsQ0FBQztRQU9sQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0Q7QUFFRCxNQUFNLE9BQU8sbUJBQW1CO0lBaUI5Qjs7UUFFSTtJQUNKLFlBQVksSUFBbUM7UUFsQi9DOzthQUVLO1FBQ0osVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUVwQjs7YUFFSztRQUNKLGVBQVUsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRTlCOztZQUVJO1FBQ0osZUFBVSxHQUFXLEVBQUUsQ0FBQztRQU92QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0Q7QUFFRCxNQUFNLE9BQU8saUJBQWlCO0lBMEM1Qjs7UUFFSTtJQUNKLFlBQVksSUFBaUM7UUEzQzdDOzthQUVLO1FBQ0osZUFBVSxHQUFTLElBQUksSUFBSSxFQUFFLENBQUM7UUFFL0I7O2FBRUs7UUFDSixRQUFHLEdBQVcsRUFBRSxDQUFDO1FBRWpCOztZQUVJO1FBQ0osaUJBQVksR0FBVyxFQUFFLENBQUM7UUFFMUI7O1lBRUk7UUFDSixnQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUV6Qjs7WUFFSTtRQUNKLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFFdkI7O1lBRUk7UUFDSixjQUFTLEdBQVcsRUFBRSxDQUFDO1FBRXZCOztZQUVJO1FBQ0osU0FBSSxHQUFXLEVBQUUsQ0FBQztRQUVsQjs7WUFFSTtRQUNKLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFPaEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNEO0FBRUQsTUFBTSxPQUFPLGNBQWM7SUFpQnpCOztRQUVJO0lBQ0osWUFBWSxJQUE4QjtRQWxCMUM7O2FBRUs7UUFDTCxnQkFBVyxHQUFTLElBQUksSUFBSSxFQUFFLENBQUM7UUFFL0I7O2FBRUs7UUFDTCxlQUFVLEdBQVcsRUFBRSxDQUFDO1FBRXZCOztZQUVJO1FBQ0osV0FBTSxHQUFXLEVBQUUsQ0FBQztRQU9uQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqKiDooajmoLzlkI3nqLHvvJpNZWRpY2FsUmVjb3JkIChNZWRpY2FsIFJlY29yZClcbiAqKiDooajmoLzoqqrmmI7vvJrpm7vlrZDnl4XmrbfmqpRcbiAqKiDnt6joqILkurrlk6HvvJrnjovppqjmmahcbiAqKiDmoKHplrHkurrlk6HvvJrlravln7nnhLZcbiAqKiDoqK3oqIjml6XmnJ/vvJpcbiAqKi9cblxuXG5leHBvcnQgY2xhc3MgU3VyZ2VyeUxpc3Qge1xuXG4gIC8qKiDmiYvooZPml6XmnJ9cbiAgICAqIEBkZWZhdWx0IG5ldyBEYXRlKClcbiAgICAqL1xuICBzdXJnZXJ5RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgLyoqIOaJi+ihk+aWueW8j1xuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICBzdXJnZXJ5V2F5OiBzdHJpbmcgPSAnJztcblxuICAvKiog6bq76YaJ5pa55byPXG4gICAgKiBAZGVmYXVsdCAnJ1xuICAgICovXG4gIGFuZXN0aGVzaWFXYXk6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiDnp5HliKVcbiAgICAqIEBkZWZhdWx0ICcnXG4gICAgKi9cbiAgZGVwYXJ0bWVudDogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOmGq+W4q1xuICAgICogQGRlZmF1bHQgJydcbiAgICAqL1xuICBkb2N0b3I6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiDlu7rmp4vlvI9cbiAgICAqIEBwYXJhbSB0aGF0IFN1cmdlcnlMaXN0XG4gICAgKi9cbiAgY29uc3RydWN0b3IodGhhdD86IFBhcnRpYWw8U3VyZ2VyeUxpc3Q+KSB7XG5cbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIHN0cnVjdHVyZWRDbG9uZSh0aGF0KSk7XG4gfVxufVxuXG5leHBvcnQgY2xhc3MgSW5zcGVjdFJlcG9ydCB7XG5cbiAgIC8qKiDni4DmhYtcbiAgICAgKiBAZGVmYXVsdCAnJ1xuICAgICAqL1xuICAgc3RhdGU6IHN0cmluZyA9ICcnO1xuXG4gICAvKiog5aCx5ZGK5ZCN56ixXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIHJlcG9ydE5hbWU6IHN0cmluZyA9ICcnO1xuXG4gICAvKiog6Ki65YilXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGRpYWdub3Npczogc3RyaW5nID0gJyc7XG5cbiAgIC8qKiDpgLLlhaXoqLrplpPmmYLplpMv5Yiw6K2355CG56uZ5pmC6ZaTL+WIsOitt+eQhuermeaZgumWky/plovlp4vml6XmnJ9cbiAgICAgKiBAZGVmYXVsdCBuZXcgRGF0ZSgpXG4gICAgICovXG4gICBwbGFubmVkU3RhcnRUaW1lOiBEYXRlID0gbmV3IERhdGUoKTtcblxuICAgLyoqIOW7uuani+W8j1xuICAgICAqIEBwYXJhbSB0aGF0IEluc3BlY3RSZXBvcnRcbiAgICAgKi9cbiAgIGNvbnN0cnVjdG9yKHRoYXQ/OiBQYXJ0aWFsPEluc3BlY3RSZXBvcnQ+KSB7XG5cbiAgICAgT2JqZWN0LmFzc2lnbih0aGlzLCBzdHJ1Y3R1cmVkQ2xvbmUodGhhdCkpO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBNZWRpY2FsRGlhZ25vc2lzIHtcblxuICAvKiog6Ki65pa36aGe5YilXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGRpYWdub3Npc1NvcnQ6IHN0cmluZyA9ICcnO1xuXG4gICAvKiog6Ki65pa35Luj56K8XG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGRpYWdub3N0aWNDb2RlOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIOiHqOW6iuiouuaWt1xuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICBjbGluaWNhbERpYWdub3Npczogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOW7uuani+W8j1xuICAgICogQHBhcmFtIHRoYXQgTWVkaWNhbERpYWdub3Npc1xuICAgICovXG4gIGNvbnN0cnVjdG9yKHRoYXQ/OiBQYXJ0aWFsPE1lZGljYWxEaWFnbm9zaXM+KSB7XG5cbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIHN0cnVjdHVyZWRDbG9uZSh0aGF0KSk7XG4gfVxufVxuXG5leHBvcnQgY2xhc3MgQ2hlY2tpbmdSZXBvcnQge1xuXG4gICAvKiog54uA5oWLXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIHN0YXRlOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIOWgseWRiuWQjeeosVxuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICByZXBvcnROYW1lOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIOeUs+iri+mgheebrlxuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICBhcHBseVByb2plY3Q6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiDlu7rmp4vlvI9cbiAgICAqIEBwYXJhbSB0aGF0IENoZWNraW5nUmVwb3J0XG4gICAgKi9cbiAgY29uc3RydWN0b3IodGhhdD86IFBhcnRpYWw8Q2hlY2tpbmdSZXBvcnQ+KSB7XG5cbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIHN0cnVjdHVyZWRDbG9uZSh0aGF0KSk7XG4gfVxufVxuXG5leHBvcnQgY2xhc3MgQ3VycmVudE1lZGljYXRpb25zIHtcblxuICAvKiog6Jel5ZOB5a245ZCNXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGRydWdOYW1lOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIOWKkemHj+WWruS9jVxuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICBkb3NlOiBudW1iZXIgPSAwO1xuXG4gICAvKiog5L2/55So5YqR6YePXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGRvc2FnZTogc3RyaW5nID0gJyc7XG5cbiAgIC8qKiDpoLvnjodcbiAgICAgKiBAZGVmYXVsdCAnJ1xuICAgICAqL1xuICAgZnJlcXVlbmN5OiBzdHJpbmcgPSAnJztcblxuIC8qKiDlu7rmp4vlvI9cbiAgICogQHBhcmFtIHRoYXQgQ3VycmVudE1lZGljYXRpb25zXG4gICAqL1xuIGNvbnN0cnVjdG9yKHRoYXQ/OiBQYXJ0aWFsPEN1cnJlbnRNZWRpY2F0aW9ucz4pIHtcblxuICAgT2JqZWN0LmFzc2lnbih0aGlzLCBzdHJ1Y3R1cmVkQ2xvbmUodGhhdCkpO1xufVxufVxuXG5leHBvcnQgY2xhc3MgTnVyc2luZ1JlY29yZCB7XG5cbiAgLyoqIOe0gOmMhOaZgumWk1xuICAgICAqIEBkZWZhdWx0IG5ldyBEYXRlKClcbiAgICAgKi9cbiAgIHJlY29yZFRpbWU6IERhdGUgPSBuZXcgRGF0ZSgpO1xuXG4gICAvKiog54Sm6bueXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIGZvY3VzOiBzdHJpbmcgPSAnJztcblxuICAvKiog5bu65qeL5byPXG4gICAgKiBAcGFyYW0gdGhhdCBOdXJzaW5nUmVjb3JkXG4gICAgKi9cbiAgY29uc3RydWN0b3IodGhhdD86IFBhcnRpYWw8TnVyc2luZ1JlY29yZD4pIHtcblxuICAgIE9iamVjdC5hc3NpZ24odGhpcywgc3RydWN0dXJlZENsb25lKHRoYXQpKTtcbiB9XG59XG5cbmV4cG9ydCBjbGFzcyBDb25zdWx0YXRpb25SZWNvcmRzIHtcblxuICAvKiog54uA5oWLXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIHN0YXRlOiBzdHJpbmcgPSAnJztcblxuICAvKiog57SA6YyE5pmC6ZaTXG4gICAgICogQGRlZmF1bHQgbmV3IERhdGUoKVxuICAgICAqL1xuICAgcmVjb3JkVGltZTogRGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgIC8qKiDnp5HliKVcbiAgICAgKiBAZGVmYXVsdCAnJ1xuICAgICAqL1xuICAgZGVwYXJ0bWVudDogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOW7uuani+W8j1xuICAgICogQHBhcmFtIHRoYXQgQ29uc3VsdGF0aW9uUmVjb3Jkc1xuICAgICovXG4gIGNvbnN0cnVjdG9yKHRoYXQ/OiBQYXJ0aWFsPENvbnN1bHRhdGlvblJlY29yZHM+KSB7XG5cbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIHN0cnVjdHVyZWRDbG9uZSh0aGF0KSk7XG4gfVxufVxuXG5leHBvcnQgY2xhc3MgRXhhbWluZUN1bXVsYXRpdmUge1xuXG4gIC8qKiDloLHlkYrml6XmnJ9cbiAgICAgKiBAZGVmYXVsdCBuZXcgRGF0ZSgpXG4gICAgICovXG4gICByZXBvcnREYXRlOiBEYXRlID0gbmV3IERhdGUoKTtcblxuICAvKiogV0JDXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIFdCQzogc3RyaW5nID0gJyc7XG5cbiAgIC8qKiBOZW90cm9waGlsaWNcbiAgICAgKiBAZGVmYXVsdCAnJ1xuICAgICAqL1xuICAgTmVvdHJvcGhpbGljOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIEx5bXBob2N5dGVzXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIEx5bXBob2N5dGVzOiBzdHJpbmcgPSAnJztcblxuICAgLyoqIE1vbm9jeXRlc1xuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICBNb25vY3l0ZXM6IHN0cmluZyA9ICcnO1xuXG4gICAvKiogQmFzb3BoaWxzXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIEJhc29waGlsczogc3RyaW5nID0gJyc7XG5cbiAgIC8qKiBOUkJDXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIE5SQkM6IHN0cmluZyA9ICcnO1xuXG4gICAvKiogTVBWXG4gICAgICogQGRlZmF1bHQgJydcbiAgICAgKi9cbiAgIE1QVjogc3RyaW5nID0gJyc7XG5cbiAgLyoqIOW7uuani+W8j1xuICAgICogQHBhcmFtIHRoYXQgRXhhbWluZUN1bXVsYXRpdmVcbiAgICAqL1xuICBjb25zdHJ1Y3Rvcih0aGF0PzogUGFydGlhbDxFeGFtaW5lQ3VtdWxhdGl2ZT4pIHtcblxuICAgIE9iamVjdC5hc3NpZ24odGhpcywgc3RydWN0dXJlZENsb25lKHRoYXQpKTtcbiB9XG59XG5cbmV4cG9ydCBjbGFzcyBNZWRpY2FsUmVjb3JkcyB7XG5cbiAgLyoqIOWwsemGq+aXpeacn1xuICAgICAqIEBkZWZhdWx0IG5ldyBEYXRlKClcbiAgICAgKi9cbiAgbWVkaWNhbERhdGU6IERhdGUgPSBuZXcgRGF0ZSgpO1xuXG4gIC8qKiDnp5HliKVcbiAgICAgKiBAZGVmYXVsdCAnJ1xuICAgICAqL1xuICBkZXBhcnRtZW50OiBzdHJpbmcgPSAnJztcblxuICAgLyoqIOS4u+ayu+mGq+eUn1xuICAgICAqIEBkZWZhdWx0ICcnXG4gICAgICovXG4gICBkb2N0b3I6IHN0cmluZyA9ICcnO1xuXG4gIC8qKiDlu7rmp4vlvI9cbiAgICAqIEBwYXJhbSB0aGF0IE1lZGljYWxSZWNvcmRzXG4gICAgKi9cbiAgY29uc3RydWN0b3IodGhhdD86IFBhcnRpYWw8TWVkaWNhbFJlY29yZHM+KSB7XG5cbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIHN0cnVjdHVyZWRDbG9uZSh0aGF0KSk7XG4gfVxufVxuIl19