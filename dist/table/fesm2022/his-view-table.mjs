import * as i0 from '@angular/core';
import { Component, Input } from '@angular/core';
import * as i2 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i1 from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ExtandDialogComponent } from '@his-directive/extand-dialog/dist/extand-dialog';
import '@his-base/date-extension';
import * as i3 from 'primeng/button';
import { ButtonModule } from 'primeng/button';
import * as i5 from 'primeng/table';
import { TableModule } from 'primeng/table';
import * as i6 from 'primeng/dropdown';
import { DropdownModule } from 'primeng/dropdown';
import * as i7 from 'primeng/tooltip';
import { TooltipModule } from 'primeng/tooltip';
import * as i8 from 'primeng/scrollpanel';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import * as i4 from 'primeng/api';

const primeGroup = [ButtonModule, TableModule, DropdownModule, TooltipModule, ScrollPanelModule];
class TableComponent {
    constructor() {
        /** card標題 */
        this.cardHeader = '';
        /** 表格標題 & 資料 */
        this.tableHeaders = [];
        this.tableList = [];
        /** 日期格式 */
        this.dateFormatString = '';
        /** 下拉式選單資料 */
        this.options = [];
        this.isVisible = false;
    }
    /**
     * 點選右上角按紐跳出視窗
     * @param selectedTemplate
     */
    onExtandDialog(selectedTemplate) {
        this.isVisible = true;
        this.selectedTemplate = selectedTemplate;
    }
    /**
     * 將日期轉成要呈現的資料型態
     * @param tableList
     * @returns 轉換日期格式的結果
     */
    keys(tableList) {
        return Object.values(tableList).map(x => x instanceof Date ? x.formatString(this.dateFormatString) : x);
    }
    /**
     * 鼠標靠近表格呈現詳細內容
     * @param tableList
     * @returns 表格詳細內容
     */
    tips(tableList) {
        let tip = '';
        if (this.tableHeaders && Array.isArray(this.tableHeaders)) {
            this.tableHeaders.forEach((header, index) => {
                const value = Object.values(tableList)[index];
                if (value !== undefined) {
                    if (value instanceof Date) {
                        const formattedDate = value.formatString(this.dateFormatString);
                        tip += `${header}: ${formattedDate}\n`;
                    }
                    else {
                        tip += `${header}: ${value}\n`;
                    }
                }
            });
        }
        return tip;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.2.3", ngImport: i0, type: TableComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "16.2.3", type: TableComponent, isStandalone: true, selector: "his-table", inputs: { cardHeader: "cardHeader", tableHeaders: "tableHeaders", tableList: "tableList", dateFormatString: "dateFormatString", options: "options" }, ngImport: i0, template: "<div class=\"my card\">\n  <div class=\"text-title\">\n    <p-dropdown [options]=\"options\" [(ngModel)]=\"selectedOption\" [filter]=\"true\" filterBy=\"display\" [showClear]=\"true\" [placeholder]=\"cardHeader\">\n      <ng-template pTemplate=\"selectedItem\">\n        <div class=\"flex align-items-center gap-2\" *ngIf=\"selectedOption\">\n          <div>{{ selectedOption.display }}</div>\n        </div>\n      </ng-template>\n      <ng-template let-option pTemplate=\"item\">\n        <div class=\"flex align-items-center gap-2\">\n          <div>{{ option.display }}</div>\n        </div>\n      </ng-template>\n    </p-dropdown>\n    <p-button icon=\"pi pi-window-maximize\" styleClass=\"p-button-rounded p-button-text\" (click)=\"onExtandDialog(tableTemplate)\"></p-button>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"tableList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n            <ng-container *ngIf=\"tableHeaders\">\n              <ng-container *ngFor=\"let header of tableHeaders.slice(0, 3)\">\n                <th>{{ header }}</th>\n              </ng-container>\n            </ng-container>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n            <ng-container *ngFor=\"let item of keys(list).slice(0, 3)\">\n              <td [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{ item }}</td>\n            </ng-container>\n          </tr>\n        </ng-template>\n    </p-table>\n  </p-scrollPanel>\n  </div>\n\n  <ng-template #tableTemplate>\n    <p-table [value]=\"tableList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of tableHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n        </ng-template>\n      </p-table>\n  </ng-template>\n  <his-extand-dialog\n    [header]=\"cardHeader\"\n    [inputTemplate]=\"selectedTemplate\"\n    [(visible)]=\"isVisible\"\n    [width]=\"'37.5rem'\"\n    [height]=\"'22.5rem'\"\n  ></his-extand-dialog>\n", styles: [":host{height:100%;width:100%}.text-title{display:flex;justify-content:space-between}.my.card{padding:.5rem;height:100%;width:100%;overflow:visible}::ng-deep .p-datatable-wrapper{overflow:unset!important;height:13vh;width:100%!important}::ng-deep .p-scrollpanel-wrapper{height:13.5vh!important}::ng-deep .p-scrollpanel-content{height:100%!important}\n"], dependencies: [{ kind: "ngmodule", type: FormsModule }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: ExtandDialogComponent, selector: "his-extand-dialog", inputs: ["inputTemplate", "header", "visible", "height", "width"], outputs: ["visibleChange"] }, { kind: "ngmodule", type: ButtonModule }, { kind: "component", type: i3.Button, selector: "p-button", inputs: ["type", "iconPos", "icon", "badge", "label", "disabled", "loading", "loadingIcon", "style", "styleClass", "badgeClass", "ariaLabel"], outputs: ["onClick", "onFocus", "onBlur"] }, { kind: "directive", type: i4.PrimeTemplate, selector: "[pTemplate]", inputs: ["type", "pTemplate"] }, { kind: "ngmodule", type: TableModule }, { kind: "component", type: i5.Table, selector: "p-table", inputs: ["frozenColumns", "frozenValue", "style", "styleClass", "tableStyle", "tableStyleClass", "paginator", "pageLinks", "rowsPerPageOptions", "alwaysShowPaginator", "paginatorPosition", "paginatorStyleClass", "paginatorDropdownAppendTo", "paginatorDropdownScrollHeight", "currentPageReportTemplate", "showCurrentPageReport", "showJumpToPageDropdown", "showJumpToPageInput", "showFirstLastIcon", "showPageLinks", "defaultSortOrder", "sortMode", "resetPageOnSort", "selectionMode", "selectionPageOnly", "contextMenuSelection", "contextMenuSelectionMode", "dataKey", "metaKeySelection", "rowSelectable", "rowTrackBy", "lazy", "lazyLoadOnInit", "compareSelectionBy", "csvSeparator", "exportFilename", "filters", "globalFilterFields", "filterDelay", "filterLocale", "expandedRowKeys", "editingRowKeys", "rowExpandMode", "scrollable", "scrollDirection", "rowGroupMode", "scrollHeight", "virtualScroll", "virtualScrollItemSize", "virtualScrollOptions", "virtualScrollDelay", "frozenWidth", "responsive", "contextMenu", "resizableColumns", "columnResizeMode", "reorderableColumns", "loading", "loadingIcon", "showLoader", "rowHover", "customSort", "showInitialSortBadge", "autoLayout", "exportFunction", "exportHeader", "stateKey", "stateStorage", "editMode", "groupRowsBy", "groupRowsByOrder", "responsiveLayout", "breakpoint", "paginatorLocale", "value", "columns", "first", "rows", "totalRecords", "sortField", "sortOrder", "multiSortMeta", "selection", "selectAll", "virtualRowHeight"], outputs: ["contextMenuSelectionChange", "selectAllChange", "selectionChange", "onRowSelect", "onRowUnselect", "onPage", "onSort", "onFilter", "onLazyLoad", "onRowExpand", "onRowCollapse", "onContextMenuSelect", "onColResize", "onColReorder", "onRowReorder", "onEditInit", "onEditComplete", "onEditCancel", "onHeaderCheckboxToggle", "sortFunction", "firstChange", "rowsChange", "onStateSave", "onStateRestore"] }, { kind: "ngmodule", type: DropdownModule }, { kind: "component", type: i6.Dropdown, selector: "p-dropdown", inputs: ["scrollHeight", "filter", "name", "style", "panelStyle", "styleClass", "panelStyleClass", "readonly", "required", "editable", "appendTo", "tabindex", "placeholder", "filterPlaceholder", "filterLocale", "inputId", "selectId", "dataKey", "filterBy", "autofocus", "resetFilterOnHide", "dropdownIcon", "optionLabel", "optionValue", "optionDisabled", "optionGroupLabel", "optionGroupChildren", "autoDisplayFirst", "group", "showClear", "emptyFilterMessage", "emptyMessage", "lazy", "virtualScroll", "virtualScrollItemSize", "virtualScrollOptions", "overlayOptions", "ariaFilterLabel", "ariaLabel", "ariaLabelledBy", "filterMatchMode", "maxlength", "tooltip", "tooltipPosition", "tooltipPositionStyle", "tooltipStyleClass", "autofocusFilter", "overlayDirection", "disabled", "itemSize", "autoZIndex", "baseZIndex", "showTransitionOptions", "hideTransitionOptions", "filterValue", "options"], outputs: ["onChange", "onFilter", "onFocus", "onBlur", "onClick", "onShow", "onHide", "onClear", "onLazyLoad"] }, { kind: "ngmodule", type: TooltipModule }, { kind: "directive", type: i7.Tooltip, selector: "[pTooltip]", inputs: ["tooltipPosition", "tooltipEvent", "appendTo", "positionStyle", "tooltipStyleClass", "tooltipZIndex", "escape", "showDelay", "hideDelay", "life", "positionTop", "positionLeft", "autoHide", "fitContent", "hideOnEscape", "pTooltip", "tooltipDisabled", "tooltipOptions"] }, { kind: "ngmodule", type: ScrollPanelModule }, { kind: "component", type: i8.ScrollPanel, selector: "p-scrollPanel", inputs: ["style", "styleClass", "step"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.2.3", ngImport: i0, type: TableComponent, decorators: [{
            type: Component,
            args: [{ selector: 'his-table', standalone: true, imports: [FormsModule, CommonModule, ExtandDialogComponent, primeGroup], template: "<div class=\"my card\">\n  <div class=\"text-title\">\n    <p-dropdown [options]=\"options\" [(ngModel)]=\"selectedOption\" [filter]=\"true\" filterBy=\"display\" [showClear]=\"true\" [placeholder]=\"cardHeader\">\n      <ng-template pTemplate=\"selectedItem\">\n        <div class=\"flex align-items-center gap-2\" *ngIf=\"selectedOption\">\n          <div>{{ selectedOption.display }}</div>\n        </div>\n      </ng-template>\n      <ng-template let-option pTemplate=\"item\">\n        <div class=\"flex align-items-center gap-2\">\n          <div>{{ option.display }}</div>\n        </div>\n      </ng-template>\n    </p-dropdown>\n    <p-button icon=\"pi pi-window-maximize\" styleClass=\"p-button-rounded p-button-text\" (click)=\"onExtandDialog(tableTemplate)\"></p-button>\n  </div>\n\n  <p-scrollPanel>\n    <p-table [value]=\"tableList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n            <ng-container *ngIf=\"tableHeaders\">\n              <ng-container *ngFor=\"let header of tableHeaders.slice(0, 3)\">\n                <th>{{ header }}</th>\n              </ng-container>\n            </ng-container>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n            <ng-container *ngFor=\"let item of keys(list).slice(0, 3)\">\n              <td [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{ item }}</td>\n            </ng-container>\n          </tr>\n        </ng-template>\n    </p-table>\n  </p-scrollPanel>\n  </div>\n\n  <ng-template #tableTemplate>\n    <p-table [value]=\"tableList\" [resizableColumns]=\"true\" [scrollable]=\"true\" scrollHeight=\"25vh\">\n      <ng-template pTemplate=\"header\">\n          <tr>\n              <th *ngFor=\"let header of tableHeaders\">{{ header }}</th>\n          </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-list >\n          <tr>\n              <td *ngFor=\"let item of keys(list)\" [pTooltip]=\"tips(list)\" tooltipPosition=\"top\">{{item}}</td>\n          </tr>\n        </ng-template>\n      </p-table>\n  </ng-template>\n  <his-extand-dialog\n    [header]=\"cardHeader\"\n    [inputTemplate]=\"selectedTemplate\"\n    [(visible)]=\"isVisible\"\n    [width]=\"'37.5rem'\"\n    [height]=\"'22.5rem'\"\n  ></his-extand-dialog>\n", styles: [":host{height:100%;width:100%}.text-title{display:flex;justify-content:space-between}.my.card{padding:.5rem;height:100%;width:100%;overflow:visible}::ng-deep .p-datatable-wrapper{overflow:unset!important;height:13vh;width:100%!important}::ng-deep .p-scrollpanel-wrapper{height:13.5vh!important}::ng-deep .p-scrollpanel-content{height:100%!important}\n"] }]
        }], propDecorators: { cardHeader: [{
                type: Input
            }], tableHeaders: [{
                type: Input
            }], tableList: [{
                type: Input
            }], dateFormatString: [{
                type: Input
            }], options: [{
                type: Input
            }] } });

/**
 ** 表格名稱：MedicalRecord (Medical Record)
 ** 表格說明：電子病歷檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/
class SurgeryList {
    /** 建構式
      * @param that SurgeryList
      */
    constructor(that) {
        /** 手術日期
          * @default new Date()
          */
        this.surgeryDate = new Date();
        /** 手術方式
          * @default ''
          */
        this.surgeryWay = '';
        /** 麻醉方式
          * @default ''
          */
        this.anesthesiaWay = '';
        /** 科別
          * @default ''
          */
        this.department = '';
        /** 醫師
          * @default ''
          */
        this.doctor = '';
        Object.assign(this, structuredClone(that));
    }
}
class InspectReport {
    /** 建構式
      * @param that InspectReport
      */
    constructor(that) {
        /** 狀態
          * @default ''
          */
        this.state = '';
        /** 報告名稱
          * @default ''
          */
        this.reportName = '';
        /** 診別
          * @default ''
          */
        this.diagnosis = '';
        /** 進入診間時間/到護理站時間/到護理站時間/開始日期
          * @default new Date()
          */
        this.plannedStartTime = new Date();
        Object.assign(this, structuredClone(that));
    }
}
class MedicalDiagnosis {
    /** 建構式
      * @param that MedicalDiagnosis
      */
    constructor(that) {
        /** 診斷類別
           * @default ''
           */
        this.diagnosisSort = '';
        /** 診斷代碼
          * @default ''
          */
        this.diagnosticCode = '';
        /** 臨床診斷
          * @default ''
          */
        this.clinicalDiagnosis = '';
        Object.assign(this, structuredClone(that));
    }
}
class CheckingReport {
    /** 建構式
      * @param that CheckingReport
      */
    constructor(that) {
        /** 狀態
          * @default ''
          */
        this.state = '';
        /** 報告名稱
          * @default ''
          */
        this.reportName = '';
        /** 申請項目
          * @default ''
          */
        this.applyProject = '';
        Object.assign(this, structuredClone(that));
    }
}
class CurrentMedications {
    /** 建構式
      * @param that CurrentMedications
      */
    constructor(that) {
        /** 藥品學名
           * @default ''
           */
        this.drugName = '';
        /** 劑量單位
          * @default ''
          */
        this.dose = 0;
        /** 使用劑量
          * @default ''
          */
        this.dosage = '';
        /** 頻率
          * @default ''
          */
        this.frequency = '';
        Object.assign(this, structuredClone(that));
    }
}
class NursingRecord {
    /** 建構式
      * @param that NursingRecord
      */
    constructor(that) {
        /** 紀錄時間
           * @default new Date()
           */
        this.recordTime = new Date();
        /** 焦點
          * @default ''
          */
        this.focus = '';
        Object.assign(this, structuredClone(that));
    }
}
class ConsultationRecords {
    /** 建構式
      * @param that ConsultationRecords
      */
    constructor(that) {
        /** 狀態
           * @default ''
           */
        this.state = '';
        /** 紀錄時間
           * @default new Date()
           */
        this.recordTime = new Date();
        /** 科別
          * @default ''
          */
        this.department = '';
        Object.assign(this, structuredClone(that));
    }
}
class ExamineCumulative {
    /** 建構式
      * @param that ExamineCumulative
      */
    constructor(that) {
        /** 報告日期
           * @default new Date()
           */
        this.reportDate = new Date();
        /** WBC
           * @default ''
           */
        this.WBC = '';
        /** Neotrophilic
          * @default ''
          */
        this.Neotrophilic = '';
        /** Lymphocytes
          * @default ''
          */
        this.Lymphocytes = '';
        /** Monocytes
          * @default ''
          */
        this.Monocytes = '';
        /** Basophils
          * @default ''
          */
        this.Basophils = '';
        /** NRBC
          * @default ''
          */
        this.NRBC = '';
        /** MPV
          * @default ''
          */
        this.MPV = '';
        Object.assign(this, structuredClone(that));
    }
}
class MedicalRecords {
    /** 建構式
      * @param that MedicalRecords
      */
    constructor(that) {
        /** 就醫日期
           * @default new Date()
           */
        this.medicalDate = new Date();
        /** 科別
           * @default ''
           */
        this.department = '';
        /** 主治醫生
          * @default ''
          */
        this.doctor = '';
        Object.assign(this, structuredClone(that));
    }
}

/*
 * Public API Surface of table
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CheckingReport, ConsultationRecords, CurrentMedications, ExamineCumulative, InspectReport, MedicalDiagnosis, MedicalRecords, NursingRecord, SurgeryList, TableComponent };
//# sourceMappingURL=his-view-table.mjs.map
