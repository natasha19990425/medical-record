import { TemplateRef } from '@angular/core';
import { Coding } from '@his-base/datatypes/dist/datatypes';
import '@his-base/date-extension';
import * as i0 from "@angular/core";
export declare class TableComponent {
    /** card標題 */
    cardHeader: string;
    /** 表格標題 & 資料 */
    tableHeaders: string[] | undefined;
    tableList: Object[];
    /** 日期格式 */
    dateFormatString: string;
    /** 下拉式選單資料 */
    options: Coding[];
    isVisible: boolean;
    selectedTemplate: TemplateRef<any>;
    selectedOption: Coding | undefined;
    /**
     * 點選右上角按紐跳出視窗
     * @param selectedTemplate
     */
    onExtandDialog(selectedTemplate: TemplateRef<any>): void;
    /**
     * 將日期轉成要呈現的資料型態
     * @param tableList
     * @returns 轉換日期格式的結果
     */
    keys(tableList: Object): any[];
    /**
     * 鼠標靠近表格呈現詳細內容
     * @param tableList
     * @returns 表格詳細內容
     */
    tips(tableList: any): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<TableComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TableComponent, "his-table", never, { "cardHeader": { "alias": "cardHeader"; "required": false; }; "tableHeaders": { "alias": "tableHeaders"; "required": false; }; "tableList": { "alias": "tableList"; "required": false; }; "dateFormatString": { "alias": "dateFormatString"; "required": false; }; "options": { "alias": "options"; "required": false; }; }, {}, never, never, true, never>;
}
