import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'his-help-tab-list',
  standalone: true,
  imports: [CommonModule,TabViewModule,ButtonModule],
  templateUrl: './help-tab-list.component.html',
  styleUrls: ['./help-tab-list.component.scss']
})
export class HelpTabListComponent {

  /**
   * 顯示於 tabView 之模板內容
   * @type {TemplateRef<any>}
   * @memberof HelpTabListComponent
   */
  @Input() inputTemplate!: TemplateRef<any>;

  /**
   * 控制 dialog 的可見性
   * @type {boolean}
   * @memberof ExtandDialogComponent
   */
  @Input() visible:boolean = false;


  @Input() tabList:any;
  @Input() widthGrow!:boolean;

  /** card標題 */
  @Input() header: string = '';

  isExpanded = false;

  tabviewHeader: string = '';

  onExpandTabList(){
    this.isExpanded = !this.isExpanded;
    console.log(this.isExpanded)
  }

  onCloseTabList(){
    this.tabList.splice(0,this.tabList.length)
  }

}
