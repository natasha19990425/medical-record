import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import '@angular/localize/init';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
//inner
import { TableComponent, SurgeryList, InspectReport, MedicalDiagnosis, CheckingReport, CurrentMedications, NursingRecord, ConsultationRecords, MedicalRecords, AdmMedicalRecord, ExamineCumulative } from '../../../table/src/public-api';
import { ChartComponent } from '../../../charts/src/public-api';
import { DisplayChartContent } from 'charts/src/lib/charts.interface';
//external
import { HeaderComponent } from '@his-directive/header/dist/header';
import { MenuTitle, SideMenuComponent} from '@his-directive/side-menu/dist/side-menu';
import { ToolBarComponent } from '@his-directive/tool-bar/dist/tool-bar';
import { Coding,Address } from '@his-base/datatypes';
//primeng
import { SplitterModule } from 'primeng/splitter';
import { TabViewModule } from 'primeng/tabview';
import { SharedService } from './shared.service';
import { ChartModule } from 'primeng/chart';
import { DividerModule } from 'primeng/divider';
import { ButtonModule } from 'primeng/button';
const primeGroup = [SplitterModule, TabViewModule, ChartModule, DividerModule, ButtonModule]

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    imports: [
        CommonModule, RouterOutlet, primeGroup,
        //inner
        TableComponent, ChartComponent,
        //external
        HeaderComponent, SideMenuComponent, ToolBarComponent, TranslateModule, HttpClientModule
    ]
})
export class AppComponent {

  headerTitle: string = '';
  title: string = '';
  chartTitle: string = '';
  tableTitle1233: string = '';
  tableTitle3311: string = '';
  tableTitle3322: string = '';
  tableTitle4411: string = '';
  tableTitle4422: string = '';
  tableTitle3333: string = '';
  tableTitle4433: string = '';
  tableTitle1444: string = '';
  medicaltableTitle3311: string = '';
  medicaltableTitle3322: string = '';
  medicaltableTitle3333: string = '';
  menuTitles: MenuTitle[] = [];
  surgeryList: SurgeryList[] = [];
  inspectReport: InspectReport[] = [];
  medicalDiagnosis: MedicalDiagnosis[] = [];
  checkingReport: CheckingReport[] = [];
  currentMedications: CurrentMedications[] =[];
  nursingRecord: NursingRecord[] = [];
  consultationRecords: ConsultationRecords[] = [];
  medicalRecords: MedicalRecords[] = [];
  displayImageContent: DisplayChartContent[] = [];
  examineCumulative: ExamineCumulative[] = [];
  menuTitleList: MenuTitle[] = [];
  chartList: Object = [];
  chartOptions: Object = [];
  dropDownList: Coding[] = [];
  patients: AdmMedicalRecord[] = [];
  currentAdmMedicalRecord!: AdmMedicalRecord ;

  isSideMenuVisible: boolean = true;
  isSearchDisable: boolean = false;

  #sharedService: SharedService = inject(SharedService);
  #translate: TranslateService = inject(TranslateService);

  constructor() {

    this.#translate.setDefaultLang(`zh-Hant`);
  }

  ngOnInit() {


    this.#initOptions();

    /** 將 menuTitle 的資料放入下拉式選單 */
    this.dropDownList = this.menuTitleList.filter(item => item.child.length > 0).flatMap(item => item.child.map(child => child.title));
  }


  /** 初始化畫面上的選項 */
  #initOptions() {
    forkJoin([
      this.#sharedService.getMenuTitles(),
      this.#sharedService.getSurgeryList(),
      this.#sharedService.getInspectReport(),
      this.#sharedService.getMedicalDiagnosis(),
      this.#sharedService.getCheckingReport(),
      this.#sharedService.getCurrentMedications(),
      this.#sharedService.getNursingRecord(),
      this.#sharedService.getConsultationRecords(),
      this.#sharedService.getMedicalRecords(),
      this.#sharedService.getExamineCumulative(),
      this.#sharedService.getDisplayImageContent(),
      this.#sharedService.getChartList(),
      this.#sharedService.getChartOptions(),
      this.#sharedService.getAdmMedicalRecords(),
    ]).subscribe(([menuTitles, surgeryList, inspectReport, medicalDiagnosis, checkingReport, currentMedications, nursingRecord,
      consultationRecords, medicalRecords, examineCumulative, displayImageContent, chartList, chartOptions,patients]) => {
      this.menuTitles = menuTitles;
      this.menuTitleList = menuTitles;
      this.surgeryList = surgeryList;
      this.inspectReport = inspectReport;
      this.medicalDiagnosis = medicalDiagnosis;
      this.checkingReport = checkingReport;
      this.currentMedications = currentMedications;
      this.nursingRecord = nursingRecord;
      this.consultationRecords = consultationRecords;
      this.medicalRecords = medicalRecords;
      this.examineCumulative = examineCumulative;
      this.displayImageContent = displayImageContent;
      this.chartList = chartList;
      this.chartOptions = chartOptions;
      this.patients = patients;
    });
  }

  /**
   * 將語系轉換為繁體中文
   */
  switchToTraditional() {
    this.#translate.use('zh-Hant');
  }

  /**
   * 將語系轉換為英文
   */
  switchToEnglish() {
    this.#translate.use('en-Us');
  }

  /**
   * 將語系轉換為簡體中文
   */
  switchToSimplified() {
    this.#translate.use('zh-Hans');
  }

  /**
   * 電話／手機 mobile 號碼文字格式化
   * @param isMobile
   * @param phone
   * @returns 格式化電話／手機號碼字串
   */
  formatPhone(isMobile: boolean, phone: number): string {

    const numberStr = phone.toString();

    if (isMobile) {
      return `0${numberStr.slice(0, 3)}-${numberStr.slice(3, 6)}-${numberStr.slice(6)}`;
    } else {
      return `0${numberStr.slice(0, -8)}-${numberStr.slice(-8)}`
    }
  }

  /**
   * Header 查詢欄位觸發事件
   * @param text
   */
  onHeaderSearch(text: string): void {

    this.patients = this.#sharedService.searchAdmMedicalRecord(text);
  }

  /**
   * Header 欄位切換觸發事件
   * @param row
   */
  onHeaderChange(row: AdmMedicalRecord): void {

    if (row) {
      this.headerTitle = this.#sharedService.formatHeaderTitle(row);
      this.currentAdmMedicalRecord = row;

    } else {
      this.headerTitle = '沒有相符合的資料';
      this.currentAdmMedicalRecord = new AdmMedicalRecord({ address: [{ "cityDistrict": { "code": "", "display": "" }, "street": "" }, { "cityDistrict": { "code": "", "display": "" }, "street": "" }] })
    }
  }

}
