import { Injectable, WritableSignal, inject, signal } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { chartList, chartOptions, admMedicalRecords, menuTitleList } from '../assets/mock/medical-record-list';
import { AdmMedicalRecord, CheckingReport, ConsultationRecords, CurrentMedications, ExamineCumulative, InspectReport, MedicalDiagnosis, MedicalRecords, NursingRecord, SurgeryList } from 'table/src/public-api';
import { DisplayChartContent } from 'charts/src/lib/charts.interface';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  /** 病人清單 */
  #patients: AdmMedicalRecord[] = [];
  translations: WritableSignal<Record<string, string>>  = signal({});

  #http = inject(HttpClient);

  getList(dataType: string): Observable<any> {
    const path = `/assets/mock/${dataType}.json`;
    return this.#http.get(path);
  }

  /**
   * 取得左側 side menu 資料
   * @returns menuTitleList
   */
  getMenuTitles(): Observable<MenuTitle[]> {
  // TODO: 跟後端串接資料
  return of(menuTitleList);
 }

  /**
  * 取得 table list 1233 資料
  * @returns surgeryList
   */
  async getSurgeryList(): Promise<SurgeryList[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<SurgeryList[]>(`assets/mock/surgery-list.json`);
    return await lastValueFrom(result$);
   }

  /**
  * 取得 table list 4411 資料
  * @returns inspectReport
   */
  async getInspectReport(): Promise<InspectReport[]> {
   // TODO: 跟後端串接資料
   const result$ =  this.#http.get<InspectReport[]>(`assets/mock/inspect-report.json`);
   return await lastValueFrom(result$);
  }

  /**
  * 取得 table list 3322 資料
  * @returns
   */
  async getMedicalDiagnosis(): Promise<MedicalDiagnosis[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<MedicalDiagnosis[]>(`assets/mock/medical-diagnosis.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 table list 4422 資料
  * @returns checkingReport
   */
  async getCheckingReport(): Promise<CheckingReport[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<CheckingReport[]>(`assets/mock/checking-report.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 table list 4433 資料
  * @returns currentMedications
   */
  async getCurrentMedications(): Promise<CurrentMedications[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<CurrentMedications[]>(`assets/mock/current-medications.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 table list 3333 資料
  * @returns nursingRecord
   */
  async getNursingRecord(): Promise<NursingRecord[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<NursingRecord[]>(`assets/mock/nursing-record.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 table list 1444 資料
  * @returns consultationRecords
   */
  async getConsultationRecords(): Promise<ConsultationRecords[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<ConsultationRecords[]>(`assets/mock/consultation-records.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得第二頁籤 table list 3311 資料
  * @returns medicalRecords
   */
  async getMedicalRecords(): Promise<MedicalRecords[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<MedicalRecords[]>(`assets/mock/medical-records.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得第二頁籤 table list 1444 資料
  * @returns examineCumulative
   */
  async getExamineCumulative(): Promise<ExamineCumulative[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<ExamineCumulative[]>(`assets/mock/examine-cumulative.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 image 資料
  * @returns displayImageContent
   */
  async getDisplayImageContent(): Promise<DisplayChartContent[]> {
    // TODO: 跟後端串接資料
    const result$ =  this.#http.get<DisplayChartContent[]>(`assets/mock/display-chart-content.json`);
    return await lastValueFrom(result$);
  }

  /**
  * 取得 chartList 資料
  * @returns chartList
   */
  getChartList(): Observable<Object> {
   // TODO: 跟後端串接資料
   return of(chartList);
  }

  /**
  * 取得 ChartOptions 資料
  * @returns chartOptions
  */
  getChartOptions(): Observable<Object> {
   // TODO: 跟後端串接資料
   return of(chartOptions);
  }

  /**
  * 取得 AdmMedicalRecords 資料
  */
  getAdmMedicalRecords(): Observable<AdmMedicalRecord[]> {

    this.#patients = admMedicalRecords;
    // TODO: 跟後端串接資料
    return of(admMedicalRecords);
  }

  /**
  * 組合 headerTitle 文字
  * @param admMedicalRecords
  * @returns headerTitle 文字
  */
  formatHeaderTitle(admMedicalRecord: AdmMedicalRecord): string {

    return `
    ${admMedicalRecord.patientNo}
    ${admMedicalRecord.name}
    ${admMedicalRecord.chartNo}
    ${admMedicalRecord.gender.display}
    ${admMedicalRecord.birthDate.formatString('YYYY-MM-DD')}
    (${admMedicalRecord.birthDate.toAgeString('[y]Y [m]M')})
    ${admMedicalRecord.extraInfo}`;
  }

  /**
  * 透過參數查詢，回傳對應的 Header 資料
  * @param text
  * @returns Header 資料
  */
  searchAdmMedicalRecord(text: string): AdmMedicalRecord[] {

    let searchResults = this.#patients;
    if (!text) {
      return this.#patients;
    }
    searchResults = this.#patients.filter((x) =>
    x?.chartNo.toLowerCase().includes(text.toLowerCase())
    );
    return searchResults;
  }
}
