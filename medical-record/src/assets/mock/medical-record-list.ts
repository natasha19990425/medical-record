import { AdmMedicalRecord } from '../../../../table/src/public-api';
import { MenuTitle } from "@his-directive/side-menu/dist/side-menu";
import { DisplayChartContent } from 'charts/src/lib/charts.interface';

export const menuTitleList: MenuTitle[] = [
  {
    icon: 'pi pi-chart-bar',
    title: { code: 'measurements', display: '身體測量' },
    child: [],
  },
  {
    icon: 'pi pi-folder',
    title: { code: 'inspectionReport', display: '檢驗報告檢查' },
    child: [
      new MenuTitle({
        title: { code: 'inspection', display: '檢驗報告' },
      }),
      new MenuTitle({
        title: { code: 'checking', display: '檢查報告' },
      }),
    ],
  },
  {
    icon: 'pi pi-calendar-minus',
    title: { code: 'medicalOrders', display: '醫藥囑相關紀錄' },
    child: [
      new MenuTitle({
        title: { code: 'outpatientClinic', display: '門診-就醫病歷' },
      }),
      new MenuTitle({
        title: { code: 'medicalChange', display: '醫藥囑異動紀錄' },
      }),
      new MenuTitle({
        title: { code: 'doctor', display: '醫囑紀錄' },
      }),
      new MenuTitle({
        title: { code: 'medicine', display: '藥囑紀錄' },
      }),
      new MenuTitle({
        title: { code: 'medicationHistory', display: '用藥歷程' },
      }),
      new MenuTitle({
        title: { code: 'chemotherapyDdrugs', display: '化療藥物紀錄' },
      }),
      new MenuTitle({
        title: { code: 'antibioticReview', display: '抗生素審核紀錄' },
      }),
      new MenuTitle({
        title: { code: 'priorReview', display: '事前審查紀錄' },
      }),
    ],
  },
  {
    icon: 'pi pi-book',
    title: { code: 'cases', display: '病例相關紀錄' },
    child: [],
  },
  {
    icon: 'pi pi-search',
    title: { code: 'surgery', display: '手術處置及治療' },
    child: [],
  },
  {
    icon: 'pi pi-users',
    title: { code: 'nurse', display: '護理相關紀錄' },
    child: [],
  },
];

export const displayImageContent: DisplayChartContent[] = [
  {
    recordTime: new Date('2023-07-18 06:20'),
    diagnosis: '住院',
    temperature: '36.9',
    breathe: '19',
    pulse: '89',
    SBP: '110',
    DBP: '67',
    MAP: '',
    AUG: '',
    SPO2: '99',
    GCS: '',
  },
  {
    recordTime: new Date('2023-07-18 08:17'),
    diagnosis: '住院',
    temperature: '37.1',
    breathe: '19',
    pulse: '59',
    SBP: '123',
    DBP: '67',
    MAP: '',
    AUG: '',
    SPO2: '99',
    GCS: '',
  },
  {
    recordTime: new Date('2023-07-18 10:25'),
    diagnosis: '住院',
    temperature: '36.5',
    breathe: '20',
    pulse: '77',
    SBP: '105',
    DBP: '64',
    MAP: '',
    AUG: '',
    SPO2: '98',
    GCS: '',
  }
];

/** 圖表資料 */
export const chartList = {
  labels: displayImageContent.map(x => x.recordTime.toLocaleTimeString()),
  datasets: [
    {
      label: '  溫度',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseFloat(x.temperature)),
      yAxisID: 'y1',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(51, 167, 255)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image1.svg';
        return image;
      },
    },
    {
      label: '  脈搏',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.pulse)),
      yAxisID: 'y2',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(222, 102, 178)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image2.svg';
        return image;
      },
    },
    {
      label: '  呼吸',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.breathe)),
      yAxisID: 'y3',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(249, 134, 57)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image3.svg';
        return image;
      },
    },
    {
      label: '  SBP',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.SBP)),
      yAxisID: 'y4',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(51, 160, 131)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image4.svg';
        return image;
      },
    },
    {
      label: '  DBP',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.DBP)),
      yAxisID: 'y5',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(51, 189, 183)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image5.svg';
        return image;
      },
    },
    {
      label: '  MAP',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.MAP)),
      yAxisID: 'y6',
      type: 'line',
      borderWidth: 1,
      pointRadius: 0,
      borderColor: 'rgb(101, 130, 228)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image6.svg';
        return image;
      },
    },
    {
      label: '  AUG',
      fill: false,
      tension: 0.4,
      data: displayImageContent.map(x => parseInt(x.AUG)),
      yAxisID: 'y7',
      type: 'line',
      borderWidth: 2,
      pointRadius: 0,
      borderColor: 'rgb(252, 195, 156)',
      pointStyle: function() {
        const image = new Image();
        image.src = 'assets/image7.svg';
        return image;
      },
    },
  ]
};

export const chartOptions = {
  maintainAspectRatio: false,
  aspectRatio: 0.4,
  plugins: {
    legend: {
      align: 'end',
      fullWidth: true,
      labels: {
        boxWidth: 20,
        usePointStyle: true
      },
    },
  },
  scales: {
    y1: {
      display: true,
      color: 'rgb(51, 167, 255)',
      title: {
        display: true,
        text: '溫度',
        color: 'rgb(51, 167, 255)',
        padding: 1,
        align: 'end',
      },
      ticks: {
        color: 'rgb(51, 167, 255)'
      },
      grid:{
        tickColor:'rgb(51, 167, 255)',
      },
      border:{
        color:'rgb(51, 167, 255)',
      }
    },
    y2: {
      display: true,
      color: 'rgb(222, 102, 178)',
      title: {
        display: true,
        text: '脈搏',
        align: 'end',
        padding: 1,
        color: 'rgb(222, 102, 178)',
      },
      ticks: {
        color: 'rgb(222, 102, 178)',
      },
      grid:{
        tickColor:'rgb(222, 102, 178)',
      },
      border:{
        color:'rgb(222, 102, 178)',
      }
    },
    y3: {
      display: true,
      title: {
        display: true,
        text: '呼吸',
        align: 'end',
        padding: 1,
        color: 'rgb(249, 134, 57)'
      },
      ticks: {
        color: 'rgb(249, 134, 57)',
      },
      grid:{
        tickColor:'rgb(249, 134, 57)',
      },
      border:{
        color:'rgb(249, 134, 57)',
      }
    },
    y4: {
      display: true,
      title: {
        display: true,
        text: '血壓',
        align: 'end',
        padding: 1,
        color: 'rgb(0, 136, 100)'
      },
      ticks: {
        color: 'rgb(0, 136, 100)',
      },
      grid:{
        tickColor:'rgb(0, 136, 100)',
      },
      border:{
        color:'rgb(0, 136, 100)',
      }
    },
    y5: {
      display: false,
    },
    y6: {
      display: false,
    },
    y7: {
      display: false,
    },

  }
}

export const admMedicalRecords: AdmMedicalRecord[] = [
  new AdmMedicalRecord({
    extraInfo: '復健科 住院期間：2023-07-18~迄今 住院天數：43天',
    name: '林明樺',
    patientNo: 'A5-180',
    chartNo: '0038842103',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1943/08/30'),
    idNo: 'B123456789',
    country: { code: 'TW', display: '台灣' },
    address: [
      {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
      {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
    ],
    contact: {
      name: '林青山',
      relationship: { code: '1', display: '父子' },
      mobile: 900000000,
      homePhone: 424613322,
      physicalAddress: {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
    },
  }),
  new AdmMedicalRecord({
    extraInfo: '胸腔外科 住院期間：2023-07-01~迄今 住院天數：60天',
    name: '童婷婷',
    patientNo: 'A5-185',
    chartNo: '0046161646',
    gender: { code: 'w', display: '女' },
    birthDate: new Date('2005/06/16'),
    idNo: 'L226466686',
    country: { code: 'AD', display: '安道爾公國' },
    address: [
      {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
      {
        cityDistrict: { code: '2', display: '台北市信義區' },
        street: '110台北市信義區信義路五段7號89樓',
      },
    ],
    contact: {
      name: '邱蚓',
      relationship: { code: '4', display: '母女' },
      mobile: 900256001,
      homePhone: 4294616224,
      physicalAddress: {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
    },
  }),
  new AdmMedicalRecord({
    extraInfo: '神經科 住院期間：2023-08-18~迄今 住院天數：10天',
    name: '邱蚓',
    patientNo: 'A5-330',
    chartNo: '0042131241',
    gender: { code: 'z', display: '女' },
    birthDate: new Date('1987/03/07'),
    idNo: 'L223456789',
    country: { code: 'TW', display: '台灣' },
    address: [
      {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
      {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
    ],
    contact: {
      name: '童俊男',
      relationship: { code: '5', display: '夫妻' },
      mobile: 900550123,
      homePhone: 4294616224,
      physicalAddress: {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
    },
  }),
  new AdmMedicalRecord({
    extraInfo: '胸腔外科 住院期間：2023-08-01~迄今 住院天數：27天',
    name: '童俊男',
    patientNo: 'A5-164',
    chartNo: '0058183848',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1985/08/18'),
    idNo: 'L228488888',
    country: { code: 'AE', display: '阿拉伯聯合大公國' },
    address: [
      {
        cityDistrict: { code: '3', display: '台東縣綠島鄉' },
        street: '951台東縣綠島鄉渔港路27號',
      },
      {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
    ],
    contact: {
      name: '邱蚓',
      relationship: { code: '5', display: '夫妻' },
      mobile: 900256001,
      homePhone: 4294616224,
      physicalAddress: {
        cityDistrict: { code: '4', display: '台中市北區' },
        street: '404台中市北區雙十路一段65號',
      },
    },
  }),
  new AdmMedicalRecord({
    extraInfo: '神經科 住院期間：2023-07-17~迄今 住院天數：44天',
    name: '林青山',
    patientNo: 'A5-145',
    chartNo: '0012183856',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1923/03/9'),
    idNo: 'M128486688',
    country: { code: 'TW', display: '台灣' },
    address: [
      {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
      {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
    ],
    contact: {
      name: '邱鷗',
      relationship: { code: '5', display: '夫妻' },
      mobile: 953257101,
      homePhone: 424613322,
      physicalAddress: {
        cityDistrict: { code: '1', display: '台中市西屯區' },
        street: '407台中市西屯區台灣大道二段166巷66巷8號',
      },
    },
  }),
]


