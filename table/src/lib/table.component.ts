import { Component, Input, Output, TemplateRef, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExtandDialogComponent } from '@his-directive/extand-dialog/dist/extand-dialog';
import { HelpTabListComponent } from '../../../help-tab-list/src/public-api';
import { Coding } from '@his-base/datatypes';
import '@his-base/date-extension';
//primeng
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { TooltipModule } from 'primeng/tooltip';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
const primeGroup = [ButtonModule, TableModule, DropdownModule, TooltipModule ,ScrollPanelModule]

@Component({
  selector: 'his-table',
  standalone: true,
  imports: [FormsModule, CommonModule, ExtandDialogComponent, HelpTabListComponent, TranslateModule, primeGroup],
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent{

  /** card標題 */
  @Input() tableTitle: string = '';

  /** 表格標題 & 資料 */
  @Input() tableList: Object[] = [{}];

  /** 日期格式 */
  @Input() dateFormatString: string = '';

  /** 下拉式選單資料 */
  @Input() options: Coding[] = [];

  @Output() selectedOption: Coding | undefined;

  isDialogVisible: boolean = false;
  isTabVisible: boolean = false;
  selectedTemplate!: TemplateRef<any>;
  tabTemplate!: TemplateRef<any>;
  translatedHeaders: string[] = [];

  #translate: TranslateService = inject(TranslateService);

  /**
   * 取出資料的title
   * @returns 表格標題
   */
  getTableHeaders(): any {
    if (this.tableList && this.tableList.length > 0) {
      let headerKeys = Object.keys(this.tableList[0]);
      headerKeys = headerKeys.map(x => x);
      return headerKeys;
    } else {
      return [];
    }
  }

  /**
   * 點選右上角按紐跳出視窗
   * @param selectedTemplate
   */
  onExtandDialog(selectedTemplate: TemplateRef<any>) {

    this.isDialogVisible = true;
    this.selectedTemplate = selectedTemplate;
  }

  /**
   * 點選右上角按紐跳出視窗
   * @param tabTemplate
   */
  onTabView(tabTemplate: TemplateRef<any>) {

    this.isTabVisible = true;
    this.tabTemplate = tabTemplate;
    console.log(this.tableTitle);
  }

  /**
   * 將單筆資料的所有屬性轉換成list，方便動態呈現
   * @param rowItem
   * @returns 轉換結果
   */
  formatValues(rowItem:Object){

    return Object.values(rowItem).map(x=>x instanceof Date ?(x as Date).formatString(this.dateFormatString) :x);
  }

  /**
   * 鼠標靠近表格呈現詳細內容
   * @param rowItem
   * @returns 表格詳細內容
   */
  formatTooltip(rowItem: any): string {

    let tip: string = '';
    const headers = this.getTableHeaders(); // 获取表头
    if (headers && this.tableList) {
      headers.forEach((header: any, index: any) => {
        this.#translate.get(header).subscribe((translatedHeader: string) => {
          let value = Object.values(rowItem)[index];
          if (value instanceof Date) {
            value = (value as Date).formatString(this.dateFormatString);
          }
          tip += `${translatedHeader}: ${value}\n`;
        });
      });
    }
    return tip;
  }
}
