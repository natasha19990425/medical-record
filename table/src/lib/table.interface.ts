import {OutpatientList} from '@his-viewmodel/outpatient-list'

/** 表格名稱：MedicalRecord (Medical Record)
 ** 表格說明：電子病歷資料檔
 ** 編訂人員：王馨晨
 ** 校閱人員：孫培然
 ** 設計日期：
 **/

export class SurgeryList {

  /** 手術日期
    * @default new Date()
    */
  surgeryDate: Date = new Date();

  /** 手術方式
    * @default ''
    */
  surgeryWay: string = '';

  /** 麻醉方式
    * @default ''
    */
  anesthesiaWay: string = '';

  /** 科別
    * @default ''
    */
  department: string = '';

  /** 醫師
    * @default ''
    */
  doctor: string = '';

  /** 建構式
    * @param that SurgeryList
    */
  constructor(that?: Partial<SurgeryList>) {

    Object.assign(this, structuredClone(that));
 }
}

export class InspectReport {

   /** 狀態
     * @default ''
     */
   state: string = '';

   /** 報告名稱
     * @default ''
     */
   reportName: string = '';

   /** 診別
     * @default ''
     */
   diagnosis: string = '';

   /** 進入診間時間/到護理站時間/到護理站時間/開始日期
     * @default new Date()
     */
   plannedStartTime: Date = new Date();

   /** 建構式
     * @param that InspectReport
     */
   constructor(that?: Partial<InspectReport>) {

     Object.assign(this, structuredClone(that));
  }
}

export class MedicalDiagnosis {

  /** 診斷類別
     * @default ''
     */
   diagnosisSort: string = '';

   /** 診斷代碼
     * @default ''
     */
   diagnosticCode: string = '';

   /** 臨床診斷
     * @default ''
     */
   clinicalDiagnosis: string = '';

  /** 建構式
    * @param that MedicalDiagnosis
    */
  constructor(that?: Partial<MedicalDiagnosis>) {

    Object.assign(this, structuredClone(that));
 }
}

export class CheckingReport {

   /** 狀態
     * @default ''
     */
   state: string = '';

   /** 報告名稱
     * @default ''
     */
   reportName: string = '';

   /** 申請項目
     * @default ''
     */
   applyProject: string = '';

  /** 建構式
    * @param that CheckingReport
    */
  constructor(that?: Partial<CheckingReport>) {

    Object.assign(this, structuredClone(that));
 }
}

export class CurrentMedications {

  /** 藥品學名
     * @default ''
     */
   drugName: string = '';

   /** 劑量單位
     * @default ''
     */
   dose: number = 0;

   /** 使用劑量
     * @default ''
     */
   dosage: string = '';

   /** 頻率
     * @default ''
     */
   frequency: string = '';

 /** 建構式
   * @param that CurrentMedications
   */
 constructor(that?: Partial<CurrentMedications>) {

   Object.assign(this, structuredClone(that));
}
}

export class NursingRecord {

  /** 紀錄時間
     * @default new Date()
     */
   recordTime: Date = new Date();

   /** 焦點
     * @default ''
     */
   focus: string = '';

  /** 建構式
    * @param that NursingRecord
    */
  constructor(that?: Partial<NursingRecord>) {

    Object.assign(this, structuredClone(that));
 }
}

export class ConsultationRecords {

  /** 狀態
     * @default ''
     */
   state: string = '';

  /** 紀錄時間
     * @default new Date()
     */
   recordTime: Date = new Date();

   /** 科別
     * @default ''
     */
   consultationDepartments: string = '';

  /** 建構式
    * @param that ConsultationRecords
    */
  constructor(that?: Partial<ConsultationRecords>) {

    Object.assign(this, structuredClone(that));
 }
}

export class ExamineCumulative {

  /** 報告日期
     * @default new Date()
     */
   reportDate: Date = new Date();

  /** WBC
     * @default ''
     */
   WBC: string = '';

   /** Neotrophilic
     * @default ''
     */
   Neotrophilic: string = '';

   /** Lymphocytes
     * @default ''
     */
   Lymphocytes: string = '';

   /** Monocytes
     * @default ''
     */
   Monocytes: string = '';

   /** Basophils
     * @default ''
     */
   Basophils: string = '';

   /** NRBC
     * @default ''
     */
   NRBC: string = '';

   /** MPV
     * @default ''
     */
   MPV: string = '';

  /** 建構式
    * @param that ExamineCumulative
    */
  constructor(that?: Partial<ExamineCumulative>) {

    Object.assign(this, structuredClone(that));
 }
}

export class MedicalRecords {

  /** 就醫日期
     * @default new Date()
     */
  medicalDate: Date = new Date();

  /** 科別
     * @default ''
     */
  department: string = '';

   /** 主治醫生
     * @default ''
     */
   doctor: string = '';

  /** 建構式
    * @param that MedicalRecords
    */
  constructor(that?: Partial<MedicalRecords>) {

    Object.assign(this, structuredClone(that));
 }
}

export class AdmMedicalRecord extends OutpatientList {

  /**
   * 病人編號
   * @default ''
   * @type {string}
   * @memberof AdmMedicalRecord
   */
  patientNo: string = '';

  /**
   * 其他資訊
   * @default ''
   * @type {string}
   * @memberof AdmMedicalRecord
   */
  extraInfo: string = '';

  constructor(that?: Partial<AdmMedicalRecord>) {
    super();

    Object.assign(this, structuredClone(that));

  }
}
